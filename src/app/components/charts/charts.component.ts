import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';

@Component({
  selector: 'app-charts',
  templateUrl: './charts.component.html',
  styleUrls: ['./charts.component.css']
})
export class ChartsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    this.loadChart();
  }

  loadChart() {
    var data = {
      labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
      series: [
        [30, 20, 25, 15, 12, 7, 2]
      ]
    };
    
    var options = {
      high: 50,
      axisX: {
        labelInterpolationFnc: function(value:any, index:any) {
          return index % 2 === 0 ? value : null;
        }
      }
    };
    
    new Chartist.Line('#hpsm', data, options);
    new Chartist.Bar('#jira', data, options);
    new Chartist.Bar('#jobs', data, options);
  }

}
